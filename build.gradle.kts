import io.gitlab.arturbosch.detekt.detekt
import org.gradle.api.internal.tasks.testing.junitplatform.JUnitPlatformTestFramework
import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  `build-scan`
  id("com.github.ben-manes.versions") version "0.20.0"
  id("jmfayard.github.io.gradle-kotlin-dsl-libs") version "0.2.6"
  id("org.owasp.dependencycheck") version "3.3.4"

  java
  kotlin("jvm") version "1.3.10"
  id("org.jetbrains.kotlin.kapt") version "1.3.10"

  checkstyle
  id("io.gitlab.arturbosch.detekt") version "1.0.0-RC10"

  jacoco
  id("org.jetbrains.dokka") version "0.9.17"
  `maven-publish`
}

group = "com.gitlab.dwfranken.euler"
setProperty("archivesBaseName", "euler")
version = "1.0.0-SNAPSHOT"

buildScan {
  setTermsOfServiceUrl("https://gradle.com/terms-of-service")
  setTermsOfServiceUrl("yes")
}

dependencyCheck {
  failBuildOnCVSS = 0f
}

detekt {
  config = files("config/detekt/detekt-config.yml")
}

repositories {
  mavenLocal()
  jcenter()
  mavenCentral()
}

tasks.withType(KotlinCompile::class.java).all {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "1.8" // Highest supported byte code version
  }
}

tasks.withType(JavaCompile::class.java).all {
  sourceCompatibility = "11"
  targetCompatibility = "11"
}

dependencies {
  detekt(Libs.detekt_formatting)
  detekt(Libs.detekt_cli)

  implementation(kotlin("stdlib-jdk8"))
  implementation(kotlin("reflect"))
  
  testImplementation(kotlin("test"))
  testImplementation(kotlin("test-junit5"))
  testImplementation("org.junit.jupiter:junit-jupiter-params:5.3.1")
  testRuntimeOnly(Libs.junit_jupiter_engine)
}

checkstyle {
  toolVersion = "8.12"
}

tasks.withType<Test> {
  useJUnitPlatform()
  testLogging {
    events("passed", "skipped", "failed")
  }
}

jacoco {
  toolVersion = "0.8.2"
}

val jacocoTestReport by tasks.getting(JacocoReport::class) {
  classDirectories = fileTree("$projectDir/build/classes/java, $projectDir/build/classes/kotlin")
  sourceDirectories = files("src/main/kotlin")

  reports {
    html.isEnabled = true
  }
}

val dokka by tasks.getting(DokkaTask::class) {
  outputFormat = "html"
  outputDirectory = "$buildDir/javadoc"
}

val dokkaJar by tasks.creating(Jar::class) {
  group = JavaBasePlugin.DOCUMENTATION_GROUP
  description = "Assembles Kotlin docs with Dokka"
  classifier = "javadoc"
  from(dokka)
}

publishing {
  publications {
    create("default", MavenPublication::class.java) {
      from(components["java"])
      artifact(dokkaJar)
    }
  }
}
