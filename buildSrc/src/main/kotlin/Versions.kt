import kotlin.String

/**
 * Find which updates are available by running
 *     `$ ./gradlew syncLibs`
 * This will only update the comments.
 *
 * YOU are responsible for updating manually the dependency version. */
object Versions {
    const val jackson_module_kotlin: String = "2.9.7" 

    const val com_github_ben_manes_versions_gradle_plugin: String = "0.20.0" 

    const val com_gradle_build_scan_gradle_plugin: String = "1.16" //available: "2.0.2" 

    const val com_palantir_docker_gradle_plugin: String = "0.20.1" 

    const val io_gitlab_arturbosch_detekt: String =
            "1.0.0-RC10" // exceed the version found: 1.0.0.M7.b1

    const val reactor_test: String = "3.2.2.RELEASE" 

    const val jmfayard_github_io_gradle_kotlin_dsl_libs_gradle_plugin: String = "0.2.6" 

    const val org_jetbrains_dokka_gradle_plugin: String = "0.9.17" 

    const val org_jetbrains_kotlin_jvm_gradle_plugin: String = "1.3.10" 

    const val org_jetbrains_kotlin_plugin_noarg_gradle_plugin: String = "1.3.10" 

    const val org_jetbrains_kotlin_plugin_spring_gradle_plugin: String = "1.3.10" 

    const val org_jetbrains_kotlin: String = "1.3.10" 

    const val junit_jupiter_engine: String = "5.3.1" 

    const val org_owasp_dependencycheck_gradle_plugin: String = "3.3.4" 

    const val reactor_spring: String = "1.0.1.RELEASE" 

    const val org_springframework_boot: String = "2.1.0.RELEASE" 

    const val bootstrap: String = "4.1.3" 

    const val jquery: String = "3.3.1-1" 

    /**
     *
     *   To update Gradle, edit the wrapper file at path:
     *      ./gradle/wrapper/gradle-wrapper.properties
     */
    object Gradle {
        const val runningVersion: String = "4.10.2"

        const val currentVersion: String = "4.10.2"

        const val nightlyVersion: String = "5.1-20181118000038+0000"

        const val releaseCandidate: String = "5.0-rc-3"
    }
}
