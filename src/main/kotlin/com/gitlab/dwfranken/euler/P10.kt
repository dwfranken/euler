package com.gitlab.dwfranken.euler

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * Find the sum of all the primes below two million.
 */
fun main() = problem {
  (2L until 2_000_000L)
    .sumByLong {
      when {
        it.isPrime() -> it
        else -> 0L
      }
    }
}

private inline fun <T> Iterable<T>.sumByLong(selector: (T) -> Long): Long {
  var sum = 0L
  for (element in this) {
    sum += selector(element)
  }
  return sum
}
