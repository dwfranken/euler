package com.gitlab.dwfranken.euler

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 * What is the 10 001st prime number?
 */
fun main() = problem {
  generateSequence(2) { it + 1 }
    .filter { it.isPrime() }
    .take(10_001)
    .last()
}
