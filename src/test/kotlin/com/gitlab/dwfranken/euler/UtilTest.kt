package com.gitlab.dwfranken.euler

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class UtilTest {

  @Test
  fun `test prime numbers`() {
    assertFalse(1.isPrime())
    assertTrue(2.isPrime())
    assertTrue(3.isPrime())
    assertFalse(4.isPrime())
    assertFalse(1024.isPrime())
    assertTrue(15_485_863.isPrime())
  }

  @Test
  fun `test factorization`() {
    assertEquals(listOf(2), 2.factorize())
    assertEquals(listOf(2, 3), 6.factorize())
    assertEquals(listOf(2, 3, 5, 7), 210.factorize())
    assertEquals(listOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2), 1024.factorize())
    assertEquals(listOf(101), 101.factorize())
  }
}
