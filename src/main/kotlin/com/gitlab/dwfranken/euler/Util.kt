package com.gitlab.dwfranken.euler

import kotlin.math.log2
import kotlin.math.sqrt

fun problem(function: () -> Any) {
  val start = System.currentTimeMillis()
  val result = function.invoke()
  val stop = System.currentTimeMillis()
  print("Result: $result, took ${stop - start}ms")
}

fun Int.isEven(): Boolean = this % 2 == 0
fun Long.isEven(): Boolean = this.rem(2L) == 0L
fun Int.isOdd(): Boolean = !this.isEven()
fun Long.isOdd(): Boolean = !this.isEven()
fun Int.squared(): Int = this * this
fun Long.squared(): Long = this * this
fun Int.sqrt(): Int = sqrt(this.toDouble()).toInt()
fun Long.sqrt(): Long = sqrt(this.toDouble()).toLong()

fun Int.isPrime(): Boolean =
  when (this) {
    in Int.MIN_VALUE..1 -> false
    in 2..3 -> true
    else -> {
      (2..this.sqrt()).none { this % it == 0 }
    }
  }

fun Long.isPrime(): Boolean =
  when (this) {
    in Long.MIN_VALUE..1L -> false
    in 2L..3L -> true
    else -> {
      (2L..this.sqrt()).none { this % it == 0L }
    }
  }

fun Long.factorize(): List<Long> {
  val factors = ArrayList<Long>(log2(this.toDouble()).toInt() + 1)

  (2L..this.sqrt())
    .firstOrNull { this % it == 0L }?.let {
      factors.add(it)
      factors.addAll((this / it).factorize())
    } ?: factors.add(this)

  return factors
}

fun Int.factorize(): List<Int> = this.toLong().factorize().map { it.toInt() }

fun String.isPalindrome(): Boolean = this == this.reversed()
fun Number.isPalindrome(): Boolean = this.toString().isPalindrome()
