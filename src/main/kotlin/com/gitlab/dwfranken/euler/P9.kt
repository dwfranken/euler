package com.gitlab.dwfranken.euler

/**
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 * a2 + b2 = c2
 *
 * For example, 32 + 42 = 9 + 16 = 25 = 52.
 *
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 */
fun main() = problem {
  for (a in 2L..998L) {
    for (b in 2L..998L) {
      val a2b2 = a.squared() + b.squared()
      val c = (1000L - a - b)
      if (a2b2 == (c.squared())) {
        println("a: $a, b: $b, c: $c")
       return@problem a * b * c
      }
    }
  }
}
